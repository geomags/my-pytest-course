import json

import pytest

from conftest import time_tracker
from fibonacci.dynamic import fibonacci_dynamic_v2

# pytestmark = pytest.mark.django_db

fib_url = "/fibonacci?n={}"


@pytest.mark.skip(reason="error with request")
@pytest.mark.parametrize("n_param", [0, 1, 2, 20])
def test_good_query_should_pass(time_tracker, client, n_param: int) -> None:
    response = client.get(fib_url.format(n_param))
    assert response.status_code == 200
    response_dict = json.loads(response.content)
    assert response_dict["status"] == "ok"
    assert response_dict["result"] == fibonacci_dynamic_v2(n_param)


@pytest.mark.skip(reason="error with request")
def test_negative_query_should_pass(time_tracker, client) -> None:
    response = client.get(fib_url.format(-5))
    assert response.status_code == 400
    response_dict = json.loads(response.content)
    assert response_dict["status"] == "error"
    assert response_dict["result"] == "n-number must be positive"


@pytest.mark.skip(reason="error with request")
@pytest.mark.parametrize("n_param", [2.23, "two", ""])
def test_wrong_type_query_should_pass(time_tracker, client, n_param: int) -> None:
    response = client.get(fib_url.format(n_param))
    assert response.status_code == 400
    response_dict = json.loads(response.content)
    assert response_dict["status"] == "error"
    assert response_dict["result"] == "n-parameter is not a number"
