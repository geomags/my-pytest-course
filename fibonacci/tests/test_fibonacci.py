from typing import Callable
import pytest
from fibonacci.naive import fibonacci_naive
from fibonacci.cached import fibonacci_cached, fibonacci_cached_lru
from fibonacci.dynamic import fibonacci_dynamic, fibonacci_dynamic_v2

from conftest import time_tracker

# from my_decorator import my_parametrize


# def test_naive() -> None:
#     # Wrong way to test a number of parameters
#     res = fibonacci_naive(n=0)
#     assert res == 0
#
#     res = fibonacci_naive(n=1)
#     assert res == 1
#
#     res = fibonacci_naive(n=2)
#     assert res == 1
#
#     res = fibonacci_naive(n=20)
#     assert res == 6765


@pytest.mark.parametrize("n,expected", [(0, 0), (1, 1), (2, 1), (20, 6765)])
# @my_parametrize(identifiers="n,expected", values=[(0, 0), (1, 1), (2, 1), (20, 6765)])
# # my implementation of pytest.mark.parametrize
@pytest.mark.parametrize(
    "fib_func",
    [
        fibonacci_naive,
        fibonacci_cached,
        fibonacci_cached_lru,
        fibonacci_dynamic,
        fibonacci_dynamic_v2,
    ],
)
# instead of creating two separated tests of both functions
def test_fibonacci(
    time_tracker, fib_func: Callable[[int], int], n: int, expected: int
) -> None:
    res = fib_func(n)
    assert res == expected
