from typing import Dict
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

from api.coronavstech.companies.models import Company
from api.coronavstech.companies.serializers import CompanySerializer
from fibonacci.dynamic import fibonacci_dynamic_v2


class CompanyViewSet(ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all().order_by("-last_update")
    pagination_class = PageNumberPagination


@api_view(http_method_names=["GET"])
def calc_fibonacci(request):
    try:
        query_n = int(request.GET.get("n"))
    except ValueError:
        return Response(
            {"status": "error", "result": "n-parameter is not a number"}, status=400
        )
    if query_n < 0:
        return Response(
            {"status": "error", "result": "n-number must be positive"}, status=400
        )

    result = fibonacci_dynamic_v2(query_n)
    return Response({"status": "ok", "result": result}, status=200)
