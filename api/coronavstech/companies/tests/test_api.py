import json
from typing import List

import pytest
from django.urls import reverse

from api.coronavstech.companies.models import Company

companies_url = reverse("companies-list")
pytestmark = pytest.mark.django_db


def raise_covid19_exception() -> None:
    raise ValueError("CoronaVirus Exception")


# ------------------ Test Get Companies ------------------
def test_zero_companies_should_return_empty_list(client) -> None:
    # client is a build-in fuxture of pytest_django
    response = client.get(companies_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []


@pytest.fixture
def amazon() -> Company:
    return Company.objects.create(name="Amazon")


def test_one_company_exist_should_succeed(client, amazon) -> None:
    response = client.get(companies_url)
    response_content = json.loads(response.content)[0]
    assert response.status_code == 200
    assert response_content["name"] == amazon.name
    assert response_content["status"] == "Hiring"
    assert response_content["application_link"] == ""
    assert response_content["note"] == ""

    assert len(json.loads(response.content)) == 1


# ------------------ Test Post Companies ------------------
def test_create_company_without_arguments_should_fail(client) -> None:
    response = client.post(path=companies_url)
    assert response.status_code == 400
    assert json.loads(response.content) == {"name": ["This field is required."]}


def test_create_existing_company_should_fail(client) -> None:
    Company.objects.create(name="Apple")
    response = client.post(path=companies_url, data={"name": "Apple"})
    assert response.status_code == 400
    assert json.loads(response.content) == {
        "name": ["company with this name already exists."]
    }


def test_create_company_with_name_all_fields_should_be_defaults(client) -> None:
    response = client.post(path=companies_url, data={"name": "test company name"})
    assert response.status_code == 201
    response_content = json.loads(response.content)
    assert response_content["name"] == "test company name"
    assert response_content["status"] == "Hiring"
    assert response_content["application_link"] == ""
    assert response_content["note"] == ""


def test_create_company_with_layoffs_status_should_succeed(client) -> None:
    response = client.post(
        path=companies_url, data={"name": "test company status", "status": "Layoffs"}
    )
    assert response.status_code == 201
    response_content = json.loads(response.content)
    assert response_content["status"] == "Layoffs"


def test_create_company_with_wrong_status_should_fail(client) -> None:
    response = client.post(
        path=companies_url,
        data={"name": "test company status", "status": "Wrong Status"},
    )
    assert response.status_code == 400
    response_content = json.loads(response.content)
    assert "Wrong Status" in str(response.content)
    assert "is not a valid choice" in str(response.content)


@pytest.mark.xfail
def test_should_be_ok_if_fails() -> None:
    assert 1 == 2


@pytest.mark.skip(reason="just because I can")
def test_should_be_skipped() -> None:
    assert 1 == 2


def test_raise_covid19_exception_should_pass() -> None:
    with pytest.raises(ValueError) as e:
        raise_covid19_exception()
    assert "CoronaVirus Exception" == str(e.value)


# ------------------ Learn about fixtures test ------------------

@pytest.fixture
def companies(request, company) -> List[Company]:
    companies = []
    names = request.param
    for name in names:
        companies.append(company(name=name))

    return companies


@pytest.fixture
def company(**kwargs):
    def _company_factory(**kwargs) -> Company:
        company_name = kwargs.pop("name", "Test Company Inc")
        return Company.objects.create(name=company_name, **kwargs)

    return _company_factory


@pytest.mark.parametrize("companies", 
[["TikTok", "Twitch", "Test Company INC"], ["Facebook", "Instagram"]],
ids=["3 T Companies", "Zuckerberg's companies"], 
indirect=True)
def test_multiple_companies_exists_should_succeed(client, companies) -> None:
    company_names = set(map(lambda x: x.name, companies))
    response_companies = client.get(companies_url).json()
    assert len(company_names) == len(response_companies)
    response_companies_names = set(
        map(lambda company: company.get("name"), response_companies)
    )
    assert company_names == response_companies_names
    