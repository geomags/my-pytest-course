# REIMPLEMENTED USING NATIVE PYTEST IN TEST_API.PY

# import json
# from unittest import TestCase

# import pytest
# from django.test import Client
# from django.urls import reverse

# from api.coronavstech.companies.models import Company


# def raise_covid19_exception() -> None:
#     raise ValueError("CoronaVirus Exception")


# @pytest.mark.django_db
# class BasicCompanyAPiTestCase(TestCase):
#     def setUp(self) -> None:
#         self.client = Client()
#         self.companies_url = reverse("companies-list")

#     def tearDown(self) -> None:
#         pass


# @pytest.mark.django_db
# class TestGetCompanies(BasicCompanyAPiTestCase):
#     def test_zero_companies_should_return_empty_list(self) -> None:
#         # companies_url = "http://127.0.0.1:8000/companies/"
#         # {basename}-list
#         response = self.client.get(self.companies_url)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(json.loads(response.content), [])

#     def test_one_company_exist_should_succeed(self) -> None:
#         test_company = Company.objects.create(name="Amazon")
#         response = self.client.get(self.companies_url)
#         response_content = json.loads(response.content)[0]
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(response_content["name"], test_company.name)
#         self.assertEqual(response_content["status"], "Hiring")
#         self.assertEqual(response_content["application_link"], "")
#         self.assertEqual(response_content["note"], "")

#         self.assertEqual(len(json.loads(response.content)), 1)

#         test_company.delete()


# class TestPostCompanies(BasicCompanyAPiTestCase):
#     def test_create_company_without_arguments_should_fail(self) -> None:
#         response = self.client.post(path=self.companies_url)
#         self.assertEqual(response.status_code, 400)
#         self.assertEqual(
#             json.loads(response.content), {"name": ["This field is required."]}
#         )

#     def test_create_existing_company_should_fail(self) -> None:
#         Company.objects.create(name="Apple")
#         response = self.client.post(path=self.companies_url, data={"name": "Apple"})
#         self.assertEqual(response.status_code, 400)
#         self.assertEqual(
#             json.loads(response.content),
#             {"name": ["company with this name already exists."]},
#         )

#     def test_create_company_with_name_all_fields_should_be_defaults(self) -> None:
#         response = self.client.post(
#             path=self.companies_url, data={"name": "test company name"}
#         )
#         self.assertEqual(response.status_code, 201)
#         response_content = json.loads(response.content)
#         self.assertEqual(response_content["name"], "test company name")
#         self.assertEqual(response_content["status"], "Hiring")
#         self.assertEqual(response_content["application_link"], "")
#         self.assertEqual(response_content["note"], "")

#     def test_create_company_with_layoffs_status_should_succeed(self) -> None:
#         response = self.client.post(
#             path=self.companies_url,
#             data={"name": "test company status", "status": "Layoffs"},
#         )
#         self.assertEqual(response.status_code, 201)
#         response_content = json.loads(response.content)
#         self.assertEqual(response_content["status"], "Layoffs")

#     def test_create_company_with_wrong_status_should_fail(self) -> None:
#         response = self.client.post(
#             path=self.companies_url,
#             data={"name": "test company status", "status": "Wrong Status"},
#         )
#         self.assertEqual(response.status_code, 400)
#         response_content = json.loads(response.content)
#         self.assertIn("Wrong Status", str(response.content))
#         self.assertIn("is not a valid choice", str(response.content))

#     @pytest.mark.xfail
#     def test_should_be_ok_if_fails(self) -> None:
#         self.assertEqual(1, 2)

#     @pytest.mark.skip(reason="just because I can")
#     def test_should_be_skipped(self) -> None:
#         sefl.assertEqual(1, 2)

#     def test_raise_covid19_exception_should_pass(self) -> None:
#         with pytest.raises(ValueError) as e:
#             raise_covid19_exception()
#         self.assertEqual("CoronaVirus Exception", str(e.value))
